package com.epam.app.ping_pong;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PingPong implements Runnable {
    private static Logger logger = LogManager.getLogger(PingPong.class);
    private String name;
    private static final Object object = new Object();

    PingPong(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        synchronized (object) {
            for (int i = 0; i < 10; i++) {
             logger.info(name + " " + i + '\n');
                object.notify();
                try {
                    object.wait();

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

