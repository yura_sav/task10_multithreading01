package com.epam.app.fibonacci;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.*;

public class ExecutorExample {
    private static Logger logger = LogManager.getLogger(ExecutorExample.class);

    public static void main(String[] args) throws InterruptedException {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {
            for (int i = 4; i < 8; i++) {
                new Fibonacci(i).run();
            }
        });
        executor.shutdown();
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Future<Long> future = executorService.submit(new Fibonacci(5).new FibonacciCall());
        Future<Long> future1 = executorService.submit(new Fibonacci(10).new FibonacciCall());

        try {
            logger.info("sum: " + future.get());
            logger.info("sum: " + future1.get());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        executorService.shutdown();
        ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        Fibonacci fibonacci = new Fibonacci(6);
        scheduledExecutorService.schedule(fibonacci, 3, TimeUnit.SECONDS);
        System.out.println();
        scheduledExecutorService.scheduleWithFixedDelay(fibonacci, 0, 1, TimeUnit.SECONDS);
        scheduledExecutorService.awaitTermination(4, TimeUnit.SECONDS);

        scheduledExecutorService.shutdown();
        boolean terminated = scheduledExecutorService.awaitTermination(1, TimeUnit.SECONDS);

        if (terminated) {
            logger.info("The executor was successfully stopped");
        } else {
            logger.info("Timeout elapsed before termination");
        }
    }
}
