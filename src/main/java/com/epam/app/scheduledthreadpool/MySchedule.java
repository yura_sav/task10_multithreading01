package com.epam.app.scheduledthreadpool;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;
import java.util.concurrent.Callable;

public class MySchedule implements Callable {
    private static Logger logger = LogManager.getLogger(MySchedule.class);

    @Override
    public Integer call() {
        Random random = new Random();
        int secondsToSleep = random.nextInt(10) + 1;
        try {
            Thread.sleep(secondsToSleep * 1000);
            logger.info("time of sleeping: " + secondsToSleep + "s");
        } catch (InterruptedException ie) {
            Thread.currentThread().interrupt();
        }
        return secondsToSleep;
    }
}
