package com.epam.app.criticalsections;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDateTime;

public class CriticalClass {
    private static Logger logger = LogManager.getLogger(CriticalClass.class);
    private static final Object object = new Object();
    private static final Object anotherObject = new Object();
    private static final Object anotherObject1 = new Object();
    private static int count = 0;

    void method1() {
        synchronized (object) {
            for (int i = 0; i < 100000; i++) {
                count++;
            }
            logger.info(count + " " + Thread.currentThread().getName());
            logger.info(LocalDateTime.now());
        }
    }

    void method2() {
        synchronized (object) {
            for (int i = 0; i < 100000; i++) {
                count++;
            }
            logger.info(count + " " + Thread.currentThread().getName());
            logger.info(LocalDateTime.now());
        }
    }

    void method3() {
        synchronized (object) {
            for (int i = 0; i < 100000; i++) {
                count++;
            }
            logger.info(count + " " + Thread.currentThread().getName());
            logger.info(LocalDateTime.now());
        }
    }

    void anotherMethod1() {
        synchronized (object) {
            logger.info("method1" + LocalDateTime.now());
        }
    }

    void anotherMethod2() {
        synchronized (anotherObject1) {
            logger.info("method2" + LocalDateTime.now());
        }
    }

    void anotherMethod3() {

        synchronized (anotherObject) {
            logger.info("method3" + LocalDateTime.now());
        }
    }
}
